package io.blueharvest.labs.gcp.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class HelloAppEngineApplication {
	public static void main(String[] args) {
		SpringApplication.run(HelloAppEngineApplication.class, args);
	}
	@GetMapping("/")
	public String hello() {
		return "Hello Spring Boot!";
	}

	@GetMapping("/greet")
	public String hello2() {
		return "Hello Spring Boot2!";
	}

}
